<?php

namespace App\Http\Livewire;

use App\Models\Kategori as ModelsKategori;
use App\Models\Buku;
use App\Models\Rak;
use Livewire\Component;
use Livewire\WithPagination;
use Illuminate\Support\Str;

class Kategori extends Component
{   
    use WithPagination;
    protected $paginationTheme = 'bootstrap';

    public $create, $edit, $delete, $nama, $kategori_id, $rak;

    protected $rules = [
        'nama' => 'required|min:5',
    ];
    

    public function create()
    {
        $this->format();
        $this->create = true;
    }

    Public function store()
    {
        $this->validate();

        ModelsKategori::create([
            'nama' => $this->nama,
            'slug' => Str::slug($this->nama)
        ]);

        session()->flash('sukses', 'Data berhasil ditambahakan');

        $this->format();
    }

    public function edit(ModelsKategori $kategori)
    {
        $this->format();

        $this->edit = true;
        $this->nama = $kategori->nama;
        $this->kategori_id = $kategori->id;
    }

    public function update(ModelsKategori $kategori)
    {
        $this->validate();

        $kategori->update([
            'nama'=> $this->nama,
            'slug'=> Str::slug($this->nama)
        ]);

        session()->flash('sukses', 'Data berhasil diubah');

        $this->format();
    }
    public function delete($id)
    {
        $this->format();
        
        $this->delete = true;
        $this->kategori_id = $id;
    }

    public function destroy (ModelsKategori $kategori)
    {
        
       // $kategori->rak()->delete();

        $rak = Rak::where('kategori_id', $kategori->id)->get();
        foreach ( $rak as $key => $value) {
            //dump($value);
            $value->update([
                'kategori_id' => 1
            ]);
        }

        $buku = Buku::where('kategori_id', $kategori->id)->get();
        foreach ( $buku as $key => $value) {
            //dump($value);
            $value->update([
                'kategori_id' => 1
            ]);
            
        }
       //die;

        $kategori->delete();
        session()->flash('sukses', 'Data berhasil dihapus');

        $this->format();
    }

    public function render()
    {
        return view('livewire.kategori', [
            'kategori' => ModelsKategori::latest()->paginate(3)
        ]);
    }

    public function format()
    {
        unset($this->kategori_id);
        unset($this->nama);
        unset($this->create);
        unset($this->edit);
        unset($this->delete);
    }
}
