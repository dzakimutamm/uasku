@extends('admin-lte/app')
@section('title', 'Buku')
@section('active-buku', 'active')

@section('content')
    <livewire:buku></livewire:buku>
@endsection