@extends('admin-lte/app')
@section('title', 'Penerbit')
@section('active-penerbit', 'active')

@section('content')
    <livewire:penerbit></livewire:penerbit>

@endsection