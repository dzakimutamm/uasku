@extends('admin-lte/app')
@section('title', 'Kategori')
@section('active-kategori', 'active')

@section('content')

    <livewire:kategori></livewire:kategori>

@endsection