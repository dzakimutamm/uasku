<?php

use App\Http\Controllers\petugas\BukuController;
use App\Http\Controllers\petugas\KategoriController;
use App\Http\Controllers\petugas\PenerbitController;
use App\Http\Controllers\petugas\RakController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/cek-role', function () {
    if (auth()->user()->hasRole(['admin', 'petugas'])) {
        return redirect('/dashboard');
    } else {
        return redirect ('/');
    }
});

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home')->middleware('auth');

Route::middleware(['auth', 'role:admin|petugas'])->group(function(){
    

    Route::get('/dashboard', function () {
        return view('petugas/dashboard');
    });

    Route::get('/kategori', KategoriController::class);
    Route::get('/rak', RakController::class);
    Route::get('/penerbit', PenerbitController::class);
    Route::get('/buku', BukuController::class);

});


